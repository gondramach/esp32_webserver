#include "stdio.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "input_iot.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"


// Từ file.h -> Khai báo function pointers
input_callback_t input_callback = NULL; // Chưa trỏ tới đâu nên cho = NULL
timeout_button_t timeoutButton_callback = NULL;

static uint64_t _start, _stop, _dem;

// 2 Khai báo function pointer để đẩy _dem ra bên ngoài xử lí
static TimerHandle_t xTimers;

static void IRAM_ATTR gpio_input_handler(void *arg)
{
    int gpio_num = (uint32_t) arg;
    uint32_t rtc = xTaskGetTickCountFromISR(); // Vào ngắt sẽ bắt đầu đếm

    if (gpio_get_level(gpio_num) == 0) // Khi bấm
    {
        _start = rtc;
        xTimerStart(xTimers,0); // Block = 0: là thời gian đợi vào queue = 0
    }
    else // Thả tay ra
    {
        xTimerStop(xTimers,0); 
        //-----------// 
        _stop = rtc;
        _dem = _stop - _start;
        input_callback(gpio_num, _dem); // function pointer đẩy ra bên ngoài
    }
}

static void vTimerCallback( TimerHandle_t xTimer )
{
    uint32_t ID;

    /* Optionally do something if the pxTimer parameter is NULL. */
    configASSERT( xTimer );

    /* The number of times this timer has expired is saved as the
    timer's ID.  Obtain the count. */
    ID = ( uint32_t ) pvTimerGetTimerID( xTimer );
    if (ID == 0)
    {
        timeoutButton_callback(BUTTON0);   
    }
    
}


// Do hàm tạo chân nút nhân nên cần phải xem là chân thứ bao nhiêu,
// cần phải bắt sườn nào
// hàm ban đầu: esp_err_t input_io_create: dùng để return về giatri
// VD : esp_err_t input_io_create(gpio_num_t gpio_num, output_level_t level)

void input_io_create(gpio_num_t gpio_num, interrupt_type_edge_t type) // Hàm create
{ // Dung API
    gpio_pad_select_gpio(gpio_num);
    gpio_set_direction(gpio_num,GPIO_MODE_INPUT);
    gpio_set_pull_mode(gpio_num,GPIO_PULLUP_ONLY);
    gpio_set_intr_type(gpio_num, type);
    gpio_install_isr_service(0); // Install service ngắt
    gpio_isr_handler_add(gpio_num, gpio_input_handler, (void*) gpio_num); // Add hàm xử lí ngắt

    xTimers = xTimerCreate
                   ( /* Just a text name, not used by the RTOS
                     kernel. */
                     "Timer_For_Time_Out",
                     /* The timer period in ticks, must be
                     greater than 0. */
                     1500 / portTICK_PERIOD_MS, // 5s
                     /* The timers will auto-reload themselves
                     when they expire. */
                     pdFALSE,
                     /* The ID is used to store a count of the
                     number of times the timer has expired, which
                     is initialised to 0. */
                     ( void * ) 0,
                     /* Each timer calls the same callback when
                     it expires. */
                     vTimerCallback 
                   );
}

int input_io_get_level(gpio_num_t gpio_num)
{
    return gpio_get_level(gpio_num);
}

// Set callback ra ngoài để xử lí
void input_set_callback(void *cb) // Con trỏ kiểu void truyền vào địa chỉ nào cũng đc
{
    input_callback = cb;
}

void input_set_timeout_callback(void *cb) // Con trỏ kiểu void truyền vào địa chỉ nào cũng đc
{
    timeoutButton_callback = cb;
}