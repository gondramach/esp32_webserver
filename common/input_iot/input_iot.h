#ifndef INPUT_IO_H
#define INPUT_IO_H
#include "esp_err.h"
#include "hal/gpio_types.h"

// Tạo function pointers
typedef void(*input_callback_t) (int, uint64_t);

// Function pointers cho TimeOut
typedef void(*timeout_button_t) (int);
#define BUTTON0 GPIO_NUM_0  

typedef enum{
    LO_TO_HI  = 1, // Low to High
    HI_TO_LO  = 2, // High to Low
    ANY_EDGE  = 3,
    LOW_LEVEL = 4,
    HIGH_LEVEL= 5,
} interrupt_type_edge_t; 

void input_io_create(gpio_num_t gpio_num, interrupt_type_edge_t type); 
int input_io_get_level(gpio_num_t gpio_num);
void input_set_callback(void *cb);
void input_set_timeout_callback(void *cb);

#endif