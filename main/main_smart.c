#include <driver/i2c.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <esp_sntp.h>
#include "sdkconfig.h"

#include <esp_http_server.h>
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "esp_smartconfig.h"
#include "esp_http_client.h"
#include "mqtt_client.h"

#include "HD44780_I2C.h"
#include "u8g2_esp32_hal.h"
#include "dht11.h"
#include "dht22.h"
#include "output_iot.h"
#include "input_iot.h"

TaskHandle_t TaskHandle_readDHT22;
TaskHandle_t TaskHandle_sntp;
// ---- Bắt đầu code html ---- //
static  httpd_handle_t server = NULL;

extern const uint8_t main_html_start[] asm("_binary_server_html_start");
extern const uint8_t main_html_end[] asm("_binary_server_html_end");
extern const uint8_t login_html_start[] asm("_binary_login_html_start");
extern const uint8_t login_html_end[] asm("_binary_login_html_end");
extern const uint8_t setting_html_start[] asm("_binary_setting_html_start");
extern const uint8_t setting_html_end[] asm("_binary_setting_html_end");

static const char *TAG1 = "example";

void save_status_led4 (const char *name, int *arr_data); // Hàm test
void save_status_led1 (const char *name, const char *key, int data); // Hàm test

// ----- 1. Get Main_Page -----
static esp_err_t main_get_handler(httpd_req_t *req)
{   
    // 3. Dùng file html
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req, (const char *)main_html_start, main_html_end - main_html_start);
    return ESP_OK;
}
static httpd_uri_t main_page = {
    .uri       = "/mainpage",
    .method    = HTTP_GET,
    .handler   = main_get_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};

// ----- 2. Get login_page -----
static esp_err_t login_get_handler(httpd_req_t *req)
{
    // 3. Dùng file html
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req, (const char *)login_html_start, login_html_end - login_html_start);
    return ESP_OK;
}
static httpd_uri_t login_page = {
    .uri       = "/login",
    .method    = HTTP_GET,
    .handler   = login_get_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};

void save_string_to_flash(const char *name, const char *key, const char *data);
char *read_string_from_flash(const char *name, const char *key);
bool check_true_nhietdo = false; // Biến dùng để sau khi đăng nhập thành công sẽ hiển thị nhiệt độ
// ----- 3. POST Username, password login -----
void login_data_callback(char *data, int len)
{
    char ssid[30] = "";
    char pass[30] = "";
    char *pt = strtok(data,",");
    if (pt != NULL)
        strcpy(ssid,pt); // Copy chuỗi pt vào ssid
    pt = strtok(NULL,","); //Ném chuỗi còn lại
    if (pt != NULL)
        strcpy(pass, pt);
    printf("User_receive: %s, pass_receive: %s\n",ssid,pass);
    
    char *b = read_string_from_flash("storage", "username");
    char *c = read_string_from_flash("storage", "password");
    char f_user[100] = ""; 
    char f_pass[100] = ""; // f_user <=> flash_username
    strcpy(f_user, b); 
    strcpy(f_pass, c);  // Xem hàm read_string_from_flash() để hiểu
    printf("\nf_user: %s\nf_pass: %s\n\n",f_user, f_pass);

    if (strcmp(ssid,f_user) == 0 && strcmp(pass,f_pass) == 0)    {
        httpd_register_uri_handler(server, &main_page);
        LCD_clearScreen();
        check_true_nhietdo = true;}
    else {
        httpd_unregister_uri_handler(server, main_page.uri, main_page.method);
        check_true_nhietdo = false;}

}
static esp_err_t login_post_handler(httpd_req_t *req)
{
    char buf[100]; // Bufer 100 phần tử -> Dùng hàm request receiver để nhận dữ liệu
    // 1.req: request | 2.buf: buffer để đọc ra | 3. Là data để đọc ra 
    httpd_req_recv(req, buf, req->content_len); 
    login_data_callback(buf, req -> content_len);
    httpd_resp_send_chunk(req, NULL, 0);  // End response
    return ESP_OK;
}

static const httpd_uri_t login_post_data = {
    .uri       = "/logininfo",
    .method    = HTTP_POST,
    .handler   = login_post_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */ 
    .user_ctx  = NULL
};
// <><>--- Kết thúc POST Username, password ---- <><>

// ---- 6. Đổi password bằng phương thức GET ---- 
void save_string_to_flash(const char *name, const char *key, const char *data);
char *read_string_from_flash(const char *name, const char *key);

static esp_err_t change_user_pass_handler(httpd_req_t *req)
{
    char resp[100];
    int buf_len = httpd_req_get_url_query_len(req) + 1;  // Bước 1: Đọc len của query
    if (buf_len > 0)
    {   // tạo buffer
        char * buf = malloc(buf_len); // malloc: cấp phát động -> cấp phát ra 1 mảng 
        if(httpd_req_get_url_query_str(req,buf,buf_len) == ESP_OK){
            char value[100];
            if(httpd_query_key_value(buf,"value", value,sizeof(value)) == ESP_OK){
                //http_rgb_button_callback(value, 1); // Do kí tự mã mãu có 6 byte. VD: FFFABC
                //printf("%s\n", value);
                char username[100] =""; 
                char old_psw[100] = "";
                char new_psw[100] = "";
                char *pt = strtok(value, "_"); // Lấy vế 3 của chuỗi 
                strcpy(username,pt); 
                pt = strtok(NULL, "_");  // Lấy vế 2 của chuỗi
                strcpy(old_psw,pt);
                pt = strtok(NULL, "_");  // Lấy vế 1 của chuỗi
                strcpy(new_psw,pt);
                printf("\nUser: %s - Old: %s - New: %s\n\n", username, old_psw, new_psw);

                // Tiến hành tính toán mật khẩu, username
                // save_string_to_flash("storage", "username", "GiaHung");
                // save_string_to_flash("storage", "password", "123");
                char *b = read_string_from_flash("storage", "username");
                char *c = read_string_from_flash("storage", "password");
                char f_user[100] = ""; 
                char f_pass[100] = ""; // f_user <=> flash_username
                strcpy(f_user, b); 
                strcpy(f_pass, c);  // Xem hàm read_string_from_flash() để hiểu
                printf("\nf_user: %s\nf_pass: %s\n\n",f_user, f_pass);

                if (strcmp(username, f_user) == 0){ // Trùng username
                    if (strcmp(old_psw, f_pass) == 0){ // Trùng pass cũ
                        printf("Tien hanh doi pass %s -> %s\n", old_psw, new_psw);
                        save_string_to_flash("storage", "password", new_psw);
                    }
                    else{ // != old_psw 
                        sprintf(resp,"%s", "Nhap lai mat khau cu");
                        httpd_resp_send(req, resp, strlen(resp));
                    }
                }
                else{ // != username
                    sprintf(resp,"%s", "Username khong khop. Vui long nhap lai !");
                    httpd_resp_send(req, resp, strlen(resp));
                }
            }
        }
        free(buf); // Giải phóng bộ nhớ
    }
    return ESP_OK;
}
static const httpd_uri_t get_ssid_pass = {
    .uri       = "/changeinfo",
    .method    = HTTP_GET,
    .handler   = change_user_pass_handler,
    .user_ctx  = NULL
};
// <><>--- Kết thúc đổi Password = phương thức GET ---- <><>

// ---- 4. Get status DHT22, 4 pin LED ----
static esp_err_t dht22_data(httpd_req_t *req)
{
    char resp[100];
    int out1,out2,out3,out4;
    out1 = gpio_get_level(GPIO_NUM_13); out2 = gpio_get_level(GPIO_NUM_12);
    out3 = gpio_get_level(GPIO_NUM_14); out4 = gpio_get_level(GPIO_NUM_27);
    sprintf(resp, "{\"Temperature\" : \"%.1f\", \"Humidity\" : \"%.1f\", \"D1\" : \"%d\", \"D2\" : \"%d\", \"D3\" : \"%d\", \"D4\" : \"%d\"}", getTemperature(), getHumidity(), out1, out2, out3, out4);
    httpd_resp_send(req, resp, strlen(resp));
    return ESP_OK;
}

static const httpd_uri_t get_status_Led_Sensor = {
    .uri       = "/getstatusLedSensor",
    .method    = HTTP_GET,
    .handler   = dht22_data,
    .user_ctx  = NULL
};

/*  5a. Bật tắt LED = GET (có thể dùng POST) ----  => Cách làm phần này tương tự như RGB
** Ý tưởng: sử dụng get?query với query (chục: là led, đon vị là on-off). VD: led?value= 10 => led 1: tắt
** Kết hợp với hàm getdata trong html để HTTP_GET ý tưởng trên 
*/ 
int list_led[4] = {GPIO_NUM_13,GPIO_NUM_12,GPIO_NUM_14,GPIO_NUM_27};
static esp_err_t led1_get_handler(httpd_req_t *req) 
{
    int buf_len = httpd_req_get_url_query_len(req) + 1;  
    if (buf_len > 0)
    {   // tạo buffer
        char * buf = malloc(buf_len);
        if(httpd_req_get_url_query_str(req,buf,buf_len) == ESP_OK){
            char value[5];
            if(httpd_query_key_value(buf,"value", value,sizeof(value)) == ESP_OK){
               
                printf("Led %d: %d\n", value[0]-48, value[1] - 48);
                output_io_set_level(list_led[value[0]-49], value[1]-48); // Chuyển char về int theo mã ascii
                
                if (gpio_get_level(GPIO_NUM_15) == 1){ // Bật led lưu
                    char key[100];
                    sprintf(key, "led%d", value[0]-48-1); // VD:  1 <=> 49 => 49 - 48 = 1 => 1 - 1= 0 => Led0
                    save_status_led1("storage", key, value[1] - 48);
                }
            }
        }
        free(buf); // Giải phóng bộ nhớ
    }
    return ESP_OK;
}
static const httpd_uri_t led1_get_data = {
    .uri       = "/ledD1",
    .method    = HTTP_GET,
    .handler   = led1_get_handler,
    .user_ctx  = NULL
};
/*  5b. Bật tắt toàn bộ led sử dụng GET Query như trên */
static esp_err_t led4_get_handler(httpd_req_t *req) 
{
    int buf_len = httpd_req_get_url_query_len(req) + 1;  
    if (buf_len > 0)
    {   // tạo buffer
        char * buf = malloc(buf_len); 
        if(httpd_req_get_url_query_str(req,buf,buf_len) == ESP_OK){
            char value[5];
            if(httpd_query_key_value(buf,"value", value,sizeof(value)) == ESP_OK){
                int temp[4];
                printf("All led: %d\n", value[0]-48);
                for (int i = 0; i <= 3; i++)
                {
                    output_io_set_level(list_led[i], value[0]-48); 
                    temp[i] = value[0] - 48;
                }
                
                //--------- Ý tưởng save: Tạo 1 mảng lưu lại biến save là = 1111 hoặc 0000 rồi ghép vào hàm save 4led ------
                if (gpio_get_level(GPIO_NUM_15) == 1) // Bật led save
                {
                    save_status_led4("storage", temp);
                }
            }
        }
        free(buf); // Giải phóng bộ nhớ
    }
    return ESP_OK;
}
static const httpd_uri_t led4_get_data = {
    .uri       = "/all4led",
    .method    = HTTP_GET,
    .handler   = led4_get_handler,
    .user_ctx  = NULL
};

/*  7a. Tạo setting page
    7 với 8 liên quan nhau:  */ 
static esp_err_t setting_get_handler(httpd_req_t *req)
{   
    // 3. Dùng file html
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req, (const char *)setting_html_start, setting_html_end - setting_html_start);
    return ESP_OK;
}
static const httpd_uri_t setting_page = {
    .uri       = "/setting",
    .method    = HTTP_GET,
    .handler   = setting_get_handler,
    .user_ctx  = NULL
};

/* 7b. Tạo liên kết để mở Setting page khi ấn Setting */
void setting_callback(char *data, int len)
{
    char buf[100];
    sprintf(buf, "%s", data);
    printf("data_receive: %s", buf);
    if (strcmp(buf,"open") == 0) {
        httpd_register_uri_handler(server, &setting_page);
    }
    else {
        httpd_unregister_uri_handler(server, setting_page.uri, setting_page.method);
    }
}
static esp_err_t open_setting_page_post_handler(httpd_req_t *req)
{
    char buf[100]; // Bufer 100 phần tử -> Dùng hàm request receiver để nhận dữ liệu
    httpd_req_recv(req, buf, req->content_len); 
    setting_callback(buf, req -> content_len);
    httpd_resp_send_chunk(req, NULL, 0);  // End response
    return ESP_OK;
}

static const httpd_uri_t open_setting_page_post_data = {
    .uri       = "/OpenSettingPage",
    .method    = HTTP_POST,
    .handler   = open_setting_page_post_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};

char string_status[30] = "";
char string_nhietdo[30] = "";
char string_doam[30] = "";
/* 8. Save setting bật đèn khi vượt ngưỡng nhiệt độ */
void settingDHT22_data_callback(char *data, int len)
{
    char *pt = strtok(data,",");
    if (pt != NULL)
        strcpy(string_status,pt); 
    pt = strtok(NULL,","); 

    if (pt != NULL)
        strcpy(string_nhietdo, pt);
    pt = strtok(NULL,",");

    if (pt != NULL)
        strcpy(string_doam,pt);
    printf("Status_receive: %s, Nhietdo: %s, Do am: %s\n",string_status, string_nhietdo, string_doam);
}

static esp_err_t settingDHT22_post_handler(httpd_req_t *req)
{
    char buf[100]; 
    httpd_req_recv(req, buf, req->content_len); 
    settingDHT22_data_callback(buf, req -> content_len);
    httpd_resp_send_chunk(req, NULL, 0);  // End response
    return ESP_OK;
}

static const httpd_uri_t settingDHT22_post_data = {
    .uri       = "/settingDHT22",
    .method    = HTTP_POST,
    .handler   = settingDHT22_post_handler,
    .user_ctx  = NULL
};


// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> //

esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err)
{   
    if (strcmp("/mainpage", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/hello URI is not available");
        /* Return ESP_OK to keep underlying socket open */
        return ESP_OK;
    } 
    if (strcmp("/setting", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "Vui long an lai Setting");
        /* Return ESP_OK to keep underlying socket open */
        return ESP_OK;
    } 
    
    /* For any other URI send 404 and close socket */
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "Nhap sai passoword. Vui long nhap lai! Some 404 error message");
    return ESP_FAIL;
}

void start_webserver(void)
{
    //httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();  // Là thư viện giúp ESP32 chạy đc HTTP
    config.lru_purge_enable = true;
    config.max_uri_handlers = 10; // Tối đa 10 uri handler

    // Start the httpd server
    ESP_LOGI(TAG1, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG1, "Registering URI handlers"); // Đăng kí những handlers
        httpd_register_uri_handler(server, &login_page);
        httpd_register_uri_handler(server, &login_post_data);
        httpd_register_uri_handler(server, &get_status_Led_Sensor);
        httpd_register_uri_handler(server, &led1_get_data); // Điều khiển 1 led
        httpd_register_uri_handler(server, &led4_get_data); // Điều khiển 4 led
        httpd_register_uri_handler(server, &get_ssid_pass);
        httpd_register_uri_handler(server, &settingDHT22_post_data);
        httpd_register_uri_handler(server, &open_setting_page_post_data);
        httpd_register_err_handler(server, HTTPD_404_NOT_FOUND, http_404_error_handler);
    }
    else {
        ESP_LOGI(TAG1, "Error starting server!"); 
    }
}

void stop_webserver(void)
{
    // Stop the httpd server
    httpd_stop(server);
}
// --- Kết thúc code html ---- //

// -------- Bắt đầu code khởi tạo Wifi -------- 

/* Biến cho led, wifi station */
#define led 2
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1
static EventGroupHandle_t s_wifi_event_group;
static const char *TAG2 = "station mode";

/* Biến cho Smart Config */
static const int CONNECTED_BIT = BIT0;
static const int ESPTOUCH_DONE_BIT = BIT1;
static const char *TAG = "smartconfig_example";
/* Kết thúc khai báo biến */

static void smartconfig_example_task(void * parm);

static void event_handler(void* arg, esp_event_base_t event_base, 
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        xTaskCreate(smartconfig_example_task, "smartconfig_example_task", 4096, NULL, 3, NULL);
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        esp_wifi_connect();
        xEventGroupClearBits(s_wifi_event_group, CONNECTED_BIT);
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        xEventGroupSetBits(s_wifi_event_group, CONNECTED_BIT);
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_SCAN_DONE) {
        ESP_LOGI(TAG, "Scan done");
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_FOUND_CHANNEL) {
        ESP_LOGI(TAG, "Found channel");
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_GOT_SSID_PSWD) {
        ESP_LOGI(TAG, "Got SSID and password");

        smartconfig_event_got_ssid_pswd_t *evt = (smartconfig_event_got_ssid_pswd_t *)event_data;
        wifi_config_t wifi_config;
        uint8_t ssid[33] = { 0 };
        uint8_t password[65] = { 0 };

        bzero(&wifi_config, sizeof(wifi_config_t));
        memcpy(wifi_config.sta.ssid, evt->ssid, sizeof(wifi_config.sta.ssid));
        memcpy(wifi_config.sta.password, evt->password, sizeof(wifi_config.sta.password));
        wifi_config.sta.bssid_set = evt->bssid_set;
        if (wifi_config.sta.bssid_set == true) {
            memcpy(wifi_config.sta.bssid, evt->bssid, sizeof(wifi_config.sta.bssid));
        }

        memcpy(ssid, evt->ssid, sizeof(evt->ssid));
        memcpy(password, evt->password, sizeof(evt->password));
        ESP_LOGI(TAG, "SSID:%s", ssid);
        ESP_LOGI(TAG, "PASSWORD:%s", password);

        ESP_ERROR_CHECK( esp_wifi_disconnect() );
        ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
        esp_wifi_connect();
        
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_SEND_ACK_DONE) {
        xEventGroupSetBits(s_wifi_event_group, ESPTOUCH_DONE_BIT);
    }
}

static void initialise_wifi(void)
{    
    s_wifi_event_group = xEventGroupCreate();
    
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    esp_wifi_set_storage(WIFI_STORAGE_FLASH); // Lưu ssid và pass

    ESP_ERROR_CHECK( esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL) );
    ESP_ERROR_CHECK( esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL) );
    ESP_ERROR_CHECK( esp_event_handler_register(SC_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL) );

    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK( esp_wifi_start() );
    
    // ý tưởng: Khi vào mode smart config thì xóa đi các task ko cần thiết để không gây ra lỗi
    vTaskDelete(TaskHandle_readDHT22);
    vTaskDelete(TaskHandle_sntp);
}

static void smartconfig_example_task(void * parm)
{
    EventBits_t uxBits;
    ESP_ERROR_CHECK( esp_smartconfig_set_type(SC_TYPE_ESPTOUCH) );
    smartconfig_start_config_t cfg = SMARTCONFIG_START_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_smartconfig_start(&cfg) );
    check_true_nhietdo = false;
    LCD_clearScreen();
    LCD_setCursor(0,0); LCD_writeStr("SmartConfig Mode");

    while (1) {
        uxBits = xEventGroupWaitBits(s_wifi_event_group, CONNECTED_BIT | ESPTOUCH_DONE_BIT, true, false, portMAX_DELAY); 
        if(uxBits & CONNECTED_BIT) {
            ESP_LOGI(TAG, "WiFi Connected to ap");
            // --- Khi kết nối thành công = smart config tắt timer blink led --- //
           // xTimerStop(xTimers,0); 
            output_io_set_level(led,0);
        }
        if(uxBits & ESPTOUCH_DONE_BIT) {
            ESP_LOGI(TAG, "smartconfig over");
            esp_smartconfig_stop();
            
            // Kết nối hoàn tất nên restart lại
            fflush(stdout);
            esp_restart();
            
            //vTaskDelete(NULL);
        }
    }
}

static int dem = 0;
static void sta_event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) 
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) 
    {
        //esp_wifi_connect();
        printf("Ket noi wifi khong thanh cong\n");
        if (dem <= 4){ // Nếu kết nối lại 4 lần ko đc sẽ fail bit và cho phép thực hiên smart config
            dem++;
            printf("Ket noi lai lan %d\n", dem);
            esp_wifi_connect();
        }
        else{
            // Không set fail bit để liên tục tìm wifi
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT); 
        }
        LCD_setCursor(0,0);
        LCD_writeStr("Mat ket noi");
        LCD_setCursor(0,1);
        LCD_writeStr("Hay ket noi lai");
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG2, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);

        LCD_setCursor(0, 0);	LCD_writeStr("Nhap \"ip/login\"");
        char str[256];
        sprintf(str, "IP:%d.%d.%d.%d",  IP2STR(&event->ip_info.ip));
        LCD_setCursor(0, 1);	LCD_writeStr(str);

        printf("Ket noi wifi thanh cong\n");
    }
}

void wifi_init_sta(void)
{
    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    // Bắt event của wifi = hàm event handler
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,ESP_EVENT_ANY_ID, &sta_event_handler, NULL, NULL));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,IP_EVENT_STA_GOT_IP, &sta_event_handler, NULL, NULL));

    wifi_config_t wifi_get_config;
    esp_wifi_get_config(WIFI_IF_STA, &wifi_get_config); // Hàm API wifi_get_config đã bao gồm đầy đủ thông tin wifi
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    
    printf("ssid_get: %s\n",wifi_get_config.sta.ssid);
    printf("pass_get: %s\n",wifi_get_config.sta.password); 
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_get_config) ); // Đổi thông tin wifi  


    ESP_ERROR_CHECK(esp_wifi_start() ); // Sau khi wifi start hàm sẽ tiến hành xuống EventGroup và bắt từng sự kiện

    printf("Khoi tao station wifi thanh cong\n");
    
    // Khởi tạo EventBits
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        printf("connected to ap SSID: %s - Password: %s\n", wifi_get_config.sta.ssid, wifi_get_config.sta.password);
        //vTaskDelete(NULL); 
        //----  Khi kết nối thành công thì tắt FreeRTOS ---- //  
    } 
    else if (bits & WIFI_FAIL_BIT) {
        printf("Failed to connect to SSID: %s, password: %s\n", wifi_get_config.sta.ssid, wifi_get_config.sta.password);
    }
}

// ><><><>-------- Kết thúc code khởi tạo Wifi -------- ><><><>

// ----- Bắt đầu tạo Event Group Button ------
#define BIT_EVENT_BUTTON_PRESS_SHORT (1 << 0)
static EventGroupHandle_t xEventGroup;
#define button_pin GPIO_NUM_0

void input_event_callback(int pin, uint64_t tick) // Do hàm ngắt gọi hàm này nên hạn chế việc xử lí lâu trong ngắt nên dùng event group
{
    if (pin == button_pin) // GPIO_NUM_4
    {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;
        // 1. Chuyển tick sang ms
        int press_ms = tick * portTICK_PERIOD_MS; // Convert tick to ms
        if (press_ms < 1000) // < 1s
        {
             xEventGroupSetBitsFromISR(xEventGroup, BIT_EVENT_BUTTON_PRESS_SHORT, &xHigherPriorityTaskWoken );
        }
    }
}

void vTask_wait_event(void *pvParameters)
{
    while(1)
    {
        const TickType_t xTicksToWait = 100 / portTICK_PERIOD_MS;
        EventBits_t uxBits = xEventGroupWaitBits(
            xEventGroup,
            BIT_EVENT_BUTTON_PRESS_SHORT,
            pdTRUE, // Both with should be cleared before returning
            pdFALSE, // Don't wait both bits, either bit will do
            xTicksToWait // portMax Delay gây lôi //Guru Meditation Error: Core  0 panic'ed (InstrFetchProhibited). Exception was unhandled. nếu ấn liên tục button
        );
        if (uxBits & BIT_EVENT_BUTTON_PRESS_SHORT) 
        {
            printf("Mode Press Short (< 1s)\n");
            esp_wifi_disconnect();
            esp_wifi_stop();

            initialise_wifi();
        }
    }
}

void save_status_led1 (const char *name, const char *key, int data); // Hàm test
void button_timeout_callback(void)
{
    printf("Tran vTimer\n");
    int status_led_save = input_io_get_level(GPIO_NUM_15);
    printf("Led save: %d\n", status_led_save); 
    
    status_led_save = 1 - status_led_save;
    output_io_set_level(GPIO_NUM_15, status_led_save);
    save_status_led1("storage", "led_save", status_led_save); // Lưu vào bộ nhớ Led
}

// ><><><>-------- Kết thúc code Event Group Button -------- ><><><>

/* Ý tưởng khi vào bộ nhớ flash: sử dụng name là "storage" và key là led0,led1,led2,led3
-> Đối với key = led_save dùng để lưu trạng thái button  */
// ---- Bắt đầu code NVS_RW_Flash vào bộ nhớ flash --- //
void save_status_led1 (const char *name, const char *key, int data)
{
    // 1. Initialize NVS
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );

    // 2. Tiến hành save
    nvs_handle_t my_handle;
    err = nvs_open(name, NVS_READWRITE, &my_handle);
    if (err == ESP_OK)
    {   
        printf("Write 1 led\n");
        err = nvs_set_i32(my_handle, key, data); // VD nvs_set_i32(my_handle, led1, 1);
        err = nvs_commit(my_handle);
        printf((err == ESP_OK) ? "->Ghi thanh cong\n" : "Ghi khong thanh cong\n");
        nvs_close(my_handle); // Đóng
    }
}

int read_status_led1(esp_err_t err, const char *name, const char *key)
{
    int temp = 0;
    nvs_handle_t my_handle;
    err = nvs_open(name, NVS_READWRITE, &my_handle);
    if (err == ESP_OK)
    {
        err = nvs_get_i32(my_handle, key , &temp);
    }   
    nvs_close(my_handle);
    return temp;
}

void save_status_led4 (const char *name, int *arr_data)
{
    // 1. Initialize NVS
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );

    // 2. Tiến hành save vào mảng với key lanluot là led0, led1, led2, led3
    nvs_handle_t my_handle;
    err = nvs_open(name, NVS_READWRITE, &my_handle);
    if (err == ESP_OK)
    {
        printf("Bat dau ghi\n");
        size_t arr_len = sizeof(arr_data);
        for (int i = 0; i < arr_len; i++)
        {
            char a[100];
            sprintf(a, "led%d", i); // led1,led2
            printf("%d ", arr_data[i]);
            err = nvs_set_i32(my_handle, a, arr_data[i]);
        }
        err = nvs_commit(my_handle);
        printf((err == ESP_OK) ? "\nGhi thanh cong\n" : "Ghi khong thanh cong\n");
        nvs_close(my_handle); // Đóng
    }
}

int *read_status_led4(esp_err_t err, const char *name)
{
    // 1. Tiến hành đọc mảng 4 led từ key led0, led1, led2, led3
    nvs_handle_t my_handle;
    err = nvs_open(name, NVS_READWRITE, &my_handle);
    if (err == ESP_OK)
    {
        printf("Bat dau doc trang thai 4 led\n");
        //int kq[4];
        int* kq = (int*) calloc(4, sizeof(int));  
        for (int i = 0; i < 4; i++)
        {
            char a[100];
            sprintf(a,"led%d",i); // led1,led2
            err = nvs_get_i32(my_handle, a, &kq[i]);
            printf("%d ", kq[i]);
        }
        nvs_close(my_handle); // Đóng 
        return kq;  // Trả về mảng
    }
    else{
        int* kq = (int*) calloc(4, sizeof(int));
        kq[0] = 0; kq[1] = 0; kq[2] = 0;  kq[3] = 0;  
        return kq;
    }
}

// --> Lưu string vào flash
void save_string_to_flash(const char *name, const char *key, const char *data)
{
    // 1. Initialize NVS
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );

    // 2. Tiến hành save vào String
    nvs_handle_t my_handle;
    err = nvs_open(name, NVS_READWRITE, &my_handle);
    if (err == ESP_OK)
    {
        printf("Bat dau luu string\n");
        err = nvs_set_str(my_handle, key, data);
        err = nvs_commit(my_handle);
        printf((err == ESP_OK) ? "Ghi string thanh cong\n" : "Ghi string khong thanh cong\n");
    }
    nvs_close(my_handle); // Đóng 
}

char *read_string_from_flash(const char *name, const char *key)
{
    // 1. Initialize NVS
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );

    // 2. Tiến hành save vào String
    nvs_handle_t my_handle;
    err = nvs_open(name, NVS_READWRITE, &my_handle);
    if (err == ESP_OK)
    {
        printf("Bat dau doc string\n");
        size_t required_size;
        err = nvs_get_str(my_handle, key, NULL, &required_size);
        char *value = malloc(required_size);
        err = nvs_get_str(my_handle, key, value, &required_size); 
        return value;
    }
    else{
        nvs_close(my_handle);
        return 0;
    }
}

// ><><><>-------- Kết thúc code Lưu vào bộ nhớ Flash -------- ><><><>

// ------ Bắt đầu code cho SNTP ------
void initialize_sntp(void)
{
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_init();
}
char str_time_sntp[200] = "00:00:00";
void sntp_function(void *pvParameters)
{
    while(1)
    {   
        time_t now;
        char strftime_buf[64];
        struct tm timeinfo;
        
        time(&now);
        setenv("TZ", "CST-7", 1); // Set timezone to VietNam
        tzset();

        localtime_r(&now, &timeinfo);
        // Check: Is time set? If not, tm_year will be (1970 - 1900).
        if(timeinfo.tm_year < (2016 - 1900)){ // Thời gian báo sai ra năm 1970
            time(&now);
            localtime_r(&now, &timeinfo); // Update lại time
        }
        else{ // Check Time OK   
            strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
            printf("The current date/time in VietNam is: %s\n", strftime_buf);
            char str_thu[50], str_gio[50], str_phut[50], str_giay[50];
            int thu = timeinfo.tm_wday; int gio = timeinfo.tm_hour;  int phut = timeinfo.tm_min;   int giay = timeinfo.tm_sec;

            sprintf(str_gio, "%d", gio);
            sprintf(str_phut, "%d", phut);
            sprintf(str_giay, "%d", giay);
            // Nếu nhỏ hơn 10 thì set lại chuỗi string
            if (gio < 10){
                sprintf(str_gio, "0%d", gio);
            }
            if (phut < 10){
                sprintf(str_phut, "0%d", phut);
            }
            if (giay < 10){
                sprintf(str_giay, "0%d", giay);
            }
            switch (thu){
            case 0:
                sprintf(str_thu,"%s","Sun");  break;
            case 1:
                sprintf(str_thu,"%s","Mon");  break;
            case 2:
                sprintf(str_thu,"%s","Tue");  break;
            case 3:
                sprintf(str_thu,"%s","Wed");  break;
            case 4:
                sprintf(str_thu,"%s","Thur");  break;
            case 5:
                sprintf(str_thu,"%s","Fri");  break;
            case 6:
                sprintf(str_thu,"%s","Sat");  break;
            }
            
            sprintf(str_time_sntp, "%s %s:%s:%s", str_thu, str_gio, str_phut, str_giay);
            vTaskDelay(1000/portTICK_RATE_MS);
        }
    }
}
// ><><><>-------- Kết thúc code SNTP -------- ><><><>

// ----- Bắt đầu code https POST data lên Firebase ----- //
// Client
esp_err_t client_event_get_handler(esp_http_client_event_handle_t evt)
{
    switch (evt->event_id)
    {
    case HTTP_EVENT_ON_DATA:
        printf("Client HTTP_EVENT_ON_DATA: %.*s\n", evt->data_len, (char *)evt->data);
        break;

    default:
        break;
    }
    return ESP_OK;
}

static void client_post_rest_function(float nhietdo, float doam) // PUT nhiệt độ lên Firebase
{
    esp_http_client_config_t config_post = {
        .url = "https://esp32a-6d7f2-default-rtdb.firebaseio.com/DHT22.json?x-http-method-override=PUT",
        .method = HTTP_METHOD_POST,
        .event_handler = client_event_get_handler};
        
    esp_http_client_handle_t client = esp_http_client_init(&config_post);

    int led_nhiet = gpio_get_level(GPIO_NUM_18);
    int led_doam = gpio_get_level(GPIO_NUM_19);

    char post_data[100] = "";
    sprintf(post_data,"{\"Nhietdo\" : \"%.1f\",\"Doam\" : \"%.1f\", \"led bao nhiet do\" : \"%d\", \"led bao do am\" : \"%d\"}", nhietdo, doam, led_nhiet, led_doam);
    //char  *post_data = "{\"nhietdo\" : \"12\",\"doam\" : \"10\"}";
    esp_http_client_set_post_field(client, post_data, strlen(post_data));
    esp_http_client_set_header(client, "Content-Type", "application/json");

    esp_http_client_perform(client);
    esp_http_client_cleanup(client);
}

static void client_post_GPIO_function() // PUT GPIO lên Firebase
{
    esp_http_client_config_t config_post = {
        .url = "https://esp32a-6d7f2-default-rtdb.firebaseio.com/GPIO.json?x-http-method-override=PUT",
        .method = HTTP_METHOD_POST,
        .event_handler = client_event_get_handler};
        
    esp_http_client_handle_t client = esp_http_client_init(&config_post);

    // Đọc trạng thái của led
    int led1,led2,led3,led4;
    led1 = gpio_get_level(GPIO_NUM_13); led2 = gpio_get_level(GPIO_NUM_12);
    led3 = gpio_get_level(GPIO_NUM_14); led4 = gpio_get_level(GPIO_NUM_27);    
    
    char post_data[100] = "";
    sprintf(post_data,"{\"Led 1\" : \"%d\",\"Led 2\" : \"%d\", \"Led 3\" : \"%d\", \"Led 4\" : \"%d\"}", led1, led2, led3, led4);
    esp_http_client_set_post_field(client, post_data, strlen(post_data));
    esp_http_client_set_header(client, "Content-Type", "application/json");

    esp_http_client_perform(client);
    esp_http_client_cleanup(client);
}

void post_gpio_len_firebase()
{
    client_post_GPIO_function();
}

// ><><><>-------- Kết thúc code https POST data lên Firebase -------- ><><><>

// ------ Bắt đầu code MQTT ------- //
static const char *TAG9 = "MQTT_TCP";

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    switch (event->event_id)
    {
    case MQTT_EVENT_CONNECTED:
        ESP_LOGI(TAG9, "MQTT_EVENT_CONNECTED");
        esp_mqtt_client_subscribe(client, "hungtopic", 1);
        esp_mqtt_client_publish(client, "hungtopic", "Mach Gia Hung", 0, 1, 0);
        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(TAG9, "MQTT_EVENT_DISCONNECTED");
        break;
    case MQTT_EVENT_SUBSCRIBED:
        // ESP_LOGI(TAG9, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(TAG9, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        // ESP_LOGI(TAG9, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
    {
        //ESP_LOGI(TAG9, "MQTT_EVENT_DATA");
        //printf("TOPIC= %.*s\r\n", event->topic_len, event->topic);
        //printf("DATA = %.*s\r\n", event->data_len, event->data);
        int arr_led[4] = {GPIO_NUM_13, GPIO_NUM_12, GPIO_NUM_14, GPIO_NUM_27};
        int arr_get_led[4] = {gpio_get_level(GPIO_NUM_13), gpio_get_level(GPIO_NUM_12), gpio_get_level(GPIO_NUM_14), gpio_get_level(GPIO_NUM_27)};

        // Ý tưởng: Sau khi nhận dc data từ MQTT sẽ chuyển MQTT payload into a C style string
        char msg_buffer[20];
        int length = event->data_len;
        memcpy(msg_buffer, event->data, length);  // //copy the payload to a buffer
        msg_buffer[length] = '\0';  //terminate with a zero to convert to a C style string
        
        // Nhận được yêu cầu gửi gpio
        if(strcmp(msg_buffer,"need_gpio") == 0){
            char gpio[10];
            sprintf(gpio,"%d%d%d%d",arr_get_led[0], arr_get_led[1], arr_get_led[2], arr_get_led[3]);
            esp_mqtt_client_publish(client, "gpiotopic", gpio, 0, 1, 0);
        }

        // Led 1
        if(strcmp(msg_buffer, "Led1_on") == 0){
            output_io_set_level(arr_led[0],1);
        }
        else if(strcmp(msg_buffer, "Led1_off") == 0){
            output_io_set_level(arr_led[0],0);
        }
        // Led 2
        else if(strcmp(msg_buffer, "Led2_on") == 0){
            output_io_set_level(arr_led[1],1);
        }
        else if(strcmp(msg_buffer, "Led2_off") == 0){
            output_io_set_level(arr_led[1],0);
        }
        // Led 3
        else if(strcmp(msg_buffer, "Led3_on") == 0){
            output_io_set_level(arr_led[2],1);
        }
        else if(strcmp(msg_buffer, "Led3_off") == 0){
            output_io_set_level(arr_led[2],0);
        }
        // Led 4
        else if(strcmp(msg_buffer, "Led4_on") == 0){
            output_io_set_level(arr_led[3],1);
        }
        else if(strcmp(msg_buffer, "Led4_off") == 0){
            output_io_set_level(arr_led[3],0);
        }
        else if(strcmp(msg_buffer, "On_all") == 0){
             output_io_set_level(arr_led[0],1);  output_io_set_level(arr_led[1],1);
             output_io_set_level(arr_led[2],1);  output_io_set_level(arr_led[3],1);
        }
        else if(strcmp(msg_buffer, "Off_all") == 0){
            output_io_set_level(arr_led[0],0);  output_io_set_level(arr_led[1],0);
            output_io_set_level(arr_led[2],0);  output_io_set_level(arr_led[3],0);
        }
    }
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(TAG9, "MQTT_EVENT_ERROR");
        break;
    default:
        ESP_LOGI(TAG9, "Other event id:%d", event->event_id);
        break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG9, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data);
}

static void mqtt_app_start(void)
{
    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = "mqtt://mqtt.eclipseprojects.io",
    };
    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);
}

// ><><><>-------- Kết thúc code MQTT -------- ><><><>

static unsigned char cross_block_bits[] = {
  0xFF, 0x3F, 0x01, 0x20, 0x01, 0x20, 0x01, 0x20, 0x01, 0x20, 0x01, 0x20, 
  0xC1, 0x20, 0xC1, 0x20, 0x01, 0x20, 0x01, 0x20, 0x01, 0x20, 0x01, 0x20, 
  0x01, 0x20, 0xFF, 0x3F, };
  
// 1. Chuyển ảnh trắng đen thành ảnh XBM = link: https://www.online-utility.org/image/convert/to/XBM
// 2. Lưu ý kích thước ảnh
static unsigned char logo_ptit[] = {
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFF, 0xFF, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x80, 0xFF, 0x0F, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0xFC, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x0F, 0x80, 0xFF, 0x01, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x00, 0x00, 0xF8, 0x03, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0xC0, 0x0F, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x80, 0x0F, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x0F, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 
  0x00, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x80, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0xC0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0xE0, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x7C, 0x00, 0x00, 0x00, 
  0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x3C, 0xF8, 0x1F, 0xFC, 0x3F, 0x00, 0xC0, 0xFF, 0x07, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x3E, 0x38, 0xF8, 0xC0, 0x03, 0x00, 0x00, 0x3C, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x1F, 0x38, 0xE0, 0x81, 
  0x01, 0x30, 0x00, 0x38, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x1F, 0x38, 0xE0, 0x80, 0x01, 0x00, 0x00, 0x38, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x1F, 0x78, 0xFE, 0x80, 0x01, 0x10, 0x00, 0x38, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x80, 0x1F, 0xF8, 0x0F, 0x80, 
  0x01, 0x74, 0x00, 0x38, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x80, 
  0x1F, 0x38, 0x00, 0x80, 0x01, 0xF6, 0x00, 0x38, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x80, 0x1F, 0x38, 0x00, 0x80, 0x01, 0xF7, 0x00, 0x38, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x80, 0x1F, 0x38, 0x00, 0x80, 
  0xC3, 0xF7, 0x00, 0x38, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x80, 
  0x1F, 0x00, 0x00, 0x00, 0xE0, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x80, 0x3F, 0x00, 0x00, 0x00, 0xF8, 0x01, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x80, 0x3F, 0x00, 0x00, 0x00, 
  0x7F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x80, 
  0x7F, 0x00, 0x00, 0xC0, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0xF0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0xFE, 0x01, 0x00, 0xFE, 
  0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0xF8, 0x07, 0xC0, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 0x07, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0xF8, 0x7F, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0xF0, 
  0xFF, 0xFF, 0xFF, 0x1F, 0xFC, 0xFF, 0xFF, 0xFF, 0x0F, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0xF8, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 
  0x0F, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 
  0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x40, 0x08, 0x10, 0xA8, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x0F, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFF, 0xFF,
};

u8g2_t u8g2;
void oled_init(void)
{
	// Setup HAL
	u8g2_esp32_hal_t u8g2_esp32_hal = U8G2_ESP32_HAL_DEFAULT;
	u8g2_esp32_hal.sda = 21;
	u8g2_esp32_hal.scl = 22;
	u8g2_esp32_hal_init(u8g2_esp32_hal);
	// Init driver
	u8g2_Setup_ssd1306_i2c_128x64_noname_f(&u8g2, U8G2_R0, u8g2_esp32_i2c_byte_cb, u8g2_esp32_gpio_and_delay_cb);
	// Init address
	u8x8_SetI2CAddress(&u8g2.u8x8,0x3C);
	// Blank Screen
	u8g2_InitDisplay(&u8g2);
	u8g2_SetPowerSave(&u8g2, 0);
}

void lcd_init(void)
{
	LCD_init(0x27, 21, 22, 16, 2);
	LCD_home();
	LCD_clearScreen();
}

char str_temperature[5] = "00";
char str_humidity[5] = "00";
void lcd_oled_hienthi(void *pvParameters);
void read_DHT22(void *pvParameters);

void app_main(void)
{
	lcd_init(); 
	oled_init();

	//1. Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
    wifi_init_sta();
    initialize_sntp();

    start_webserver();
    mqtt_app_start();

	u8g2_ClearBuffer(&u8g2);
	u8g2_SetFont(&u8g2, u8g2_font_timR14_tf);
	//u8g2_DrawStr(&u8g2, 2,17,"Hello World!");

	//u8g2_DrawXBMP(&u8g2, 40, 40, 14, 14, cross_block_bits);
    u8g2_DrawXBMP(&u8g2, 0, 0, 128, 64, logo_ptit);

	u8g2_SendBuffer(&u8g2);
    vTaskDelay(3000/portTICK_RATE_MS);

	DHT22_init_io(GPIO_NUM_4);

	output_io_create(GPIO_NUM_13); //led 1
    output_io_create(GPIO_NUM_12); //led 2
    output_io_create(GPIO_NUM_14); //led 3
    output_io_create(GPIO_NUM_27); //led 4
    output_io_create(GPIO_NUM_15); //led_save 
    output_io_create(GPIO_NUM_18); // led báo nhiệt độ
    output_io_create(GPIO_NUM_19); // led báo độ ẩm
    output_io_set_level(GPIO_NUM_18, 0);
    output_io_set_level(GPIO_NUM_19, 0);

    // 2. Đọc lại trạng thái button_save
    int bt_save = read_status_led1(ret, "storage", "led_save"); // Xem lại phần ý tưởng (led_save: để lưu trạng thái led save)
    output_io_set_level(GPIO_NUM_15, bt_save);
    if (bt_save == 0) // Ko có bật mode lưu flash
    {
        output_io_set_level(GPIO_NUM_13, 0);
        output_io_set_level(GPIO_NUM_12, 0);
        output_io_set_level(GPIO_NUM_14, 0);
        output_io_set_level(GPIO_NUM_27, 0);
    }
    else{ // bt_save == 1 // Hiển thị status lại như cũ
        int *kq;
        kq = read_status_led4(ret, "storage");
        output_io_set_level(GPIO_NUM_13, kq[0]);
        output_io_set_level(GPIO_NUM_12, kq[1]);
        output_io_set_level(GPIO_NUM_14, kq[2]);
        output_io_set_level(GPIO_NUM_27, kq[3]);
    }


    input_io_create(GPIO_NUM_0, ANY_EDGE);
    input_set_callback(input_event_callback);
    input_set_timeout_callback(button_timeout_callback);
    xEventGroup = xEventGroupCreate(); // Khởi động Event Group
    xTaskCreate(vTask_wait_event, "vTask_wait_event", 4096, NULL, 4, NULL); // Độ ưu tiên = nhau để có thể đọc nhiệt độ ko bị lỗi
                                                                            //Guru Meditation Error: Core  0 panic'ed (InstrFetchProhibited). Exception was unhandled.
    // Lưu ý phải chỉnh stack lớn phòng lỗi stack overflow
	xTaskCreate(&read_DHT22, "read_DHT22", 4096, NULL, 6, &TaskHandle_readDHT22);
    xTaskCreate(&lcd_oled_hienthi, "lcd_oled_hienthi", 2048, NULL, 5, NULL);
    xTaskCreate(&sntp_function, "sntp_function", 4096, NULL, 5, &TaskHandle_sntp);
    check_true_nhietdo = false;
}

void read_DHT22(void *pvParameters)
{
    while(1)
	{
		int dht22_current_data = readDHT();
        errorHandler(dht22_current_data);
        if (dht22_current_data == ESP_OK)
        {
            printf("Nhiet do: %.1f - Do am: %.1f\n", getTemperature(), getHumidity());
            // Dùng sprintf để chuyển float thành char để hiển thị OLED
            sprintf(str_temperature, "%.1f", getTemperature());
            sprintf(str_humidity, "%.1f", getHumidity());
            client_post_rest_function(getTemperature(), getHumidity());
            post_gpio_len_firebase();

            if(strcmp(string_status, "true") == 0)
            {
                float val,val2;
                val = atof(string_nhietdo);
                val2 = atof(string_doam);
                if (getTemperature() >= val) // Nhiệt độ
                {
                    printf("Nhiet do vuot nguong\n");   
                    output_io_set_level(GPIO_NUM_18, 1);
                }
                else{
                    output_io_set_level(GPIO_NUM_18, 0);
                }
                if (getHumidity() >= val2) // Độ ẩm
                {
                    printf("Do am vuot nguong\n");   
                    output_io_set_level(GPIO_NUM_19, 1);
                }
                else{
                    output_io_set_level(GPIO_NUM_19, 0);
                }
            }
            else // string_status = "false"
            {
                output_io_set_level(GPIO_NUM_18, 0);
                output_io_set_level(GPIO_NUM_19, 0);
            }
        }
		vTaskDelay(1200 / portTICK_RATE_MS);
	}
}

void lcd_oled_hienthi(void *pvParameters)
{
    while(1){
    u8g2_ClearBuffer(&u8g2);
    u8g2_DrawStr(&u8g2, 0, 40, "Nhiet do: ");
    u8g2_DrawStr(&u8g2, 0, 60, "Do am: ");
    u8g2_DrawStr(&u8g2,80, 40, str_temperature);
    u8g2_DrawStr(&u8g2,80, 60, str_humidity);
    u8g2_DrawStr(&u8g2,10, 20, str_time_sntp); // Hiển thị thời gian lấy từ hàm sntp_function
    u8g2_SendBuffer(&u8g2);
    if (check_true_nhietdo == true)
    {
        LCD_setCursor(0, 0);	LCD_writeStr("Nhiet do: ");
        LCD_setCursor(12, 0);	LCD_writeStr(str_temperature);
        LCD_setCursor(0, 1); 	LCD_writeStr("Do am: ");
        LCD_setCursor(12,1);	LCD_writeStr(str_humidity);
    }
    }
}

